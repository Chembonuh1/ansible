FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    curl gnupg2 wget python3-distutils \
    git git-lfs sshpass python3-dev python3-pip \
    libssl-dev libffi-dev build-essential

# Import the HashiCorp GPG key
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /usr/share/keyrings/hashicorp-archive-keyring.gpg

# Add the Vagrant repository
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com buster main" | tee /etc/apt/sources.list.d/hashicorp.list

RUN apt-get update && apt-get install -y \
    vagrant \
    qemu qemu-utils libvirt-daemon-system libvirt-clients ebtables dnsmasq-base \
    libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev python3-venv && \
    rm -rf /var/cache/apt/*

RUN vagrant plugin install vagrant-libvirt

# Install GitLab Runner
RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
RUN apt-get install -y gitlab-runner

# Create a non-root user with the same UID/GID as the mounted ~/.vagrant.d folder
RUN groupadd --gid 1000 vagrant && \
    useradd --uid 1000 --gid 1000 --shell /bin/bash --create-home vagrant

# Set the VAGRANT_HOME environment variable
ENV VAGRANT_HOME=/home/vagrant/.vagrant.d

# Set the working directory to /workspace
WORKDIR /workspace

# Set the timezone
USER root
RUN ln -fs /usr/share/zoneinfo/America/Detroit /etc/localtime

COPY requirements.* ./

COPY roles/requirements.yaml ./roles/requirements.yaml

COPY collections/requirements.yaml ./collections/requirements.yaml

# Switch to the vagrant user for pip installation
#USER vagrant

RUN pip install --no-cache-dir -r requirements.txt

RUN ansible-galaxy install -r ./roles/requirements.yaml --force

RUN ansible-galaxy install -r ./collections/requirements.yaml --force

CMD ["bash"]
